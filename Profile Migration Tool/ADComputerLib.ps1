﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2017 v5.4.138
	 Created on:   	1/9/2018 2:53 PM
	 Created by:   	SSandler
	 Organization: 	
	 Filename:     	ADComputerLib.ps1
	===========================================================================
	.DESCRIPTION
		A description of the file.
#>
$ComputerList

function GetAllADComputers
{
	Import-Module activedirectory
	$ComputerList = Get-ADComputer -Filter *
	[System.Array]$ComputerNames
	
	foreach ($Comp in $ComputerList) {
		$ComputerNames.Add($Comp.Name)
	}
	return $ComputerList
}

function BuildTree ([System.Windows.Forms.TreeView]$Tree, $NodeList) {
		
		foreach ($node in $NodeList) {
			$Tree.Nodes.Add($node.Name)
	}
	
	#$Tree.Sort()
	return $Tree
}

#region Control Helper Functions
function Get-CheckedNode
{
<#
	.SYNOPSIS
		This function collects a list of checked nodes in a TreeView

	.DESCRIPTION
		This function collects a list of checked nodes in a TreeView

	.PARAMETER  $NodeCollection
		The collection of nodes to search

	.PARAMETER  $CheckedNodes
		The ArrayList that will contain the all the checked items
	
	.EXAMPLE
		$CheckedNodes = New-Object System.Collections.ArrayList
		Get-CheckedNode $treeview1.Nodes $CheckedNodes
		foreach($node in $CheckedNodes)
		{	
			Write-Host $node.Text
		}
#>
	param (
		[ValidateNotNull()]
		[System.Windows.Forms.TreeNodeCollection]$NodeCollection,
		[ValidateNotNull()]
		[System.Collections.ArrayList]$CheckedNodes
	)
	
	foreach ($Node in $NodeCollection)
	{
		if ($Node.Checked)
		{
			[void]$CheckedNodes.Add($Node)
		}
		Get-CheckedNode $Node.Nodes $CheckedNodes
	}
}