﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2017 v5.4.138
	 Created on:   	1/4/2018 2:41 PM
	 Created by:   	SSandler
	 Organization: 	CCI
	 Filename:     	ADUserLib.ps1
	===========================================================================
	.DESCRIPTION
		A description of the file.
#>


### Import List ###

<# SUMMARY
	This function uses the activedirectiory module to create a list of users in AD.
#>

$Logfile = ".\Logs\$(gc env:computername).log"
[int]$fileCount = 0
filter timestamp { "$(Get-Date -Format G): $_" }

function GetAllUsers ([bool]$IncludeDisabledUsers)
{
	#Load the users from ad
	Import-Module activedirectory
	try
	{
		if ($IncludeDisabledUsers)
		{
			$UserList = Get-ADUser -Filter *
		}
		else
		{
			$UserList = Get-ADUser -Filter 'enabled -eq $true'
		}
	}
	catch
	{
		LogError "Could not import ActiveDirectory Module." 20
	}
	return $UserList
}


<# SUMMARY
	This function passes username, password, server and domain to auto-login the user
	in order to setup the profile folders
#>

function LogInUser([string]$domain, [string]$server, [string]$un, [string]$pw)
{
	$logUser = $un | timestamp
	LogWrite '==============LOGIN===================='
	LogWrite $logUser
	LogWrite '======================================='
	$fulluser = $domain + "\" + $un
	cmdkey /generic:TERMSRV/$server /user:$fulluser /pass:$pw
	mstsc /v:$server
	Start-Sleep -Milliseconds 1500
	cmdkey /delete:TERMSRV/$server /user:$fulluser /pass:$pw
}

function TryDisconnect ([string]$un, [string]$server, [int]$Timer)
{
	$TimeOutValue = 200
	$flag = DisconnectUser $un $server
	if ($flag -eq $false -and $Timer -lt $TimeOutValue)
	{
		Start-Sleep -Seconds 5
		$Timer += 5
		TryDisconnect -un $un -server $server -Timer $Timer
	}
	else
	{
		$logEndUser = $un | timestamp
		LogWrite '==============LOGOFF==================='
		LogWrite $logEndUser
		LogWrite '======================================='
		LogWrite '======================================='
	}
}

function DisconnectUser([string]$un, [string]$server)
{
	$sessionID = qwinsta $un /server:$server
	Write-Host $sessionID
	if ($sessionID -eq $null)
	{
		return $false	
	}
	$sessionNum = $sessionID[1].subString(39, 9).Trim()
	#rwinsta $sessionNum /server:$server
	#reset session $sessionNum /server:$server
	#tsdiscon $sessionNum /server:$server
	rwinsta $sessionNum /server:$server
	return $true
}

function CheckFolderPermissions([string]$path)
{
	#get Domain information
	$userdomain = $strDomainDNS = [System.DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain().Name
	$dot = $userdomain.IndexOf(".")
	$dagroup = $userdomain.Substring(0, $dot).Trim() + '\Domain Admins'
	
	$aclresult = Get-Acl -Path $path
	
	ForEach ($Dir in $aclresult.Access)
	{
		if ($Dir.IdentityReference -eq $dagroup)
		{
			if ($Dir.FileSystemRights -eq 'FullControl')
			{
				return $true
				Write-Host $dagroup -ForegroundColor Green
			}
			else
			{
				return $false
				CheckFolderPermissions($path)
			}
		}
	}
}



function MoveProfileFolders([string]$un, [string]$sourceServer, [string]$destinationServer, [int]$Timer)
{
	
	$FoldersToSearch = @(
		'Desktop'
		'Favorites'
		'Documents'
		'Mail'
	)
	
	$SourceRoot = $sourceServer + '\' + $un
	$DestinationRoot = $destinationServer + '\' + $un
	
	$TimeOutValue = 200
	$permresult = CheckFolderPermissions($DestinationRoot)
	$permsetresult = 'Permissions set: ' + $permresult
	LogWrite $permsetresult
	
	if ($permresult -eq $false -and $Timer -lt $TimeOutValue)
	{
		Start-Sleep -Seconds 5
		$Timer += 5
		CheckFolderPermissions($DestinationRoot)
	}
	
	if ($permresult -eq $true)
	{
		$mailfolder = Join-Path -Path $DestinationRoot -ChildPath 'Mail'
		if (-not (Test-Path -Path $('filesystem::' + $mailfolder)))
		{
			md $mailfolder
		}
		#$archivefolder_root = Join-Path -Path $DestinationRoot -ChildPath 'ARCHIVE'	
		#md $archivefolder_root
		
		if (-not (Test-Path -Path $('filesystem::' + $sourceServer + '\' + $un)))
		{
			Write-Warning $un' could not be found on source: '$sourceServer.
		}
		elseif (-not (Test-Path -Path $('filesystem::' + $($destinationServer) + '\' + $un)))
		{
			Write-Warning $un' could not be found on destination: '$destinationServer.
		}
		else
		{
			SetPermissions $DestinationRoot
			SortFolders $SourceRoot $DestinationRoot $mailfolder
			SortHiddenFolders $SourceRoot $DestinationRoot $mailfolder
			LogWrite $fileCount			
		}
	}
}

function SortFolders ($path, $newpath, $mailpath)
{
	$children = Get-ChildItem $path
	
	foreach ($child in $children)
	{
		$childType = $child.GetType() | Select name
		[string]$temp = $childType.Name		
		
		switch ($temp)
		{
			
			"DirectoryInfo"
			{
				$p = $path + "\" + $child.name
				$n = $newpath + "\" + $child.name
				#$pathsProcessed = 'Processed ' + $p
				#LogWrite $pathsProcessed
				
				
				[string]$Temp = $p
				$p
				if (-not (Test-Path -Path $('filesystem::' + $n)))
				{
					md $n					
				}
				SortFolders $p $n $mailpath
			}
			"FileInfo"
			{
				
				$p = $path + "\" + $child.name
				$child.name
				$fileExtention = [System.IO.Path]::GetExtension($child)
				
				if ($fileExtention -eq '.zip' -or $fileExtention -eq '.rar' -or $fileExtention -eq '.exe' -or $fileExtention -eq '.7z')
				{
					$logdata = 'SKIP ITEM: Skipping ' + $p
					LogWrite $logdata
				}
				elseif ($fileExtention -eq '.pst')
				{
					Copy-Item -Path $p -Destination $mailpath -Force
					$logdata = 'MAIL ITEM: Moved ' + $p + ' to ' + $mailpath
					LogWrite $logdata
					CountFiles
				}
				else
				{
					$copytrue = Copy-Item -Path $p -Destination $newpath -PassThru -ErrorAction silentlyContinue
					if ($copytrue)
					{
						$copytrue
					}
					else
					{
						$errLog = "COPY FAILURE: " + $p
						LogWrite $errLog
						Write-Host $errLog
					}					
					#Copy-Item -Path $p -Destination $newpath -Force
					#$logdata = 'STANDARD ITEM: Moved ' + $p + ' to ' + $newpath					
					#CountFiles
				}
			}
			default
			{
				echo "IDK"
			}			
		}		
	}
}

function SortHiddenFolders ($path, $newpath, $mailpath)
{
	$hiddenchildren = Get-ChildItem $path -Hidden
	
	foreach ($child in $hiddenchildren)
	{
		$childType = $child.GetType() | Select name
		[string]$temp = $childType.Name
		
		switch ($temp)
		{
			
			"DirectoryInfo"
			{
				$p = $path + "\" + $child.name
				$n = $newpath + "\" + $child.name
				#$pathsProcessed = 'Processed ' + $p
				#LogWrite $pathsProcessed
				
				
				[string]$Temp = $p
				$p
				if (-not (Test-Path -Path $('filesystem::' + $n)))
				{
					md $n
				}
				$attrib = Get-Item $p -Force
				
				$isHidden = $attrib.attributes | where { $_.attributes -match "ReadOnly" }
				Write-Host $isHidden
				
				if ($isHidden -eq "Hidden")
				{
					$n.attributes = "Hidden"
				}
				SortFolders $p $n $mailpath
			}
			"FileInfo"
			{
				
				$p = $path + "\" + $child.name
				$child.name
				$fileExtention = [System.IO.Path]::GetExtension($child)
				
				if ($fileExtention -eq '.zip' -or $fileExtention -eq '.rar' -or $fileExtention -eq '.exe' -or $fileExtention -eq '.7z')
				{
					$logdata = 'SKIP ITEM: Skipping ' + $p
					LogWrite $logdata
				}
				elseif ($fileExtention -eq '.pst')
				{
					Copy-Item -Path $p -Destination $mailpath -Force
					$logdata = 'MAIL ITEM: Moved ' + $p + ' to ' + $mailpath
					LogWrite $logdata
					CountFiles
				}
				else
				{
					$copytrue = Copy-Item -Path $p -Destination $newpath -PassThru -ErrorAction silentlyContinue
					if ($copytrue)
					{
						$copytrue
						
						if ($p.attributes -eq 'Hidden')
						{
							$newpath.attributes = 'Hidden'
						}
					}
					else
					{
						$errLog = "COPY FAILURE: " + $p
						LogWrite $errLog
						Write-Host $errLog
					}
					#Copy-Item -Path $p -Destination $newpath -Force
					#$logdata = 'STANDARD ITEM: Moved ' + $p + ' to ' + $newpath					
					#CountFiles
				}
			}
			default
			{
				echo "IDK"
			}
		}
	}
}

function SetPermissions ($homepath)
{
	$AccountsToSet = @(
		'Domain Admins'
		'CCI'
		'Administrator'
	)
	
	$HomeFolders = Get-ChildItem $homepath -Directory
	
	[string]$userdomain = [System.DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain().Name
	foreach ($HomeFolder in $HomeFolders)
	{
		$Path = $HomeFolder.FullName
		$Acl = (Get-Item $Path).GetAccessControl('Access')
		$Foldername = $HomeFolder.Name
		foreach ($Account in $AccountsToSet)
		{
			$accountset = $userdomain + '\' + $Account
			$Ar = New-Object System.Security.AccessControl.FileSystemAccessRule($Account, 'FullControl', 'ContainerInherit,ObjectInherit', 'None', 'Allow')
			$Acl.SetAccessRule($Ar)
			Set-Acl -path $Path -AclObject $Acl					
		}
	}
}


Function LogWrite
{	
	Param ([string]$logstring)	
	Add-content $Logfile -value $logstring
}


Function CountFiles
{
	$fileCount++
}


#region Filtering CheckLists
<# SUMMARY [FilterList]
	Takes a checklist and String and filters the checklist based on the string input.
Requires the MainForm to contain a CheckList Object.
#>

function UpdateUserList([System.Windows.Forms.CheckedListBox]$UserCheckList, [string]$filter)
{
	$UserCheckList.Items.Clear()
	
	$UserList = GetAllUsers
	
	foreach ($user in $UserList)
	{
		[string]$temp = $user.name
		
		if ($filter -ne $null)
		{
			
			if ($temp.ToLower().Contains($filter.ToLower()))
			{
				Update-ListBox -ListBox $UserCheckList -Items $temp -Append
				$UserCheckList.Sorted = $true
			}
		}
		else
		{
			Update-ListBox -ListBox $UserCheckList -Items $temp -Append
			$UserCheckList.Sorted = $true
		}
		
	}
}
#endregion

#region Control Helper Functions
function Update-ListBox
{
<#
	.SYNOPSIS
		This functions helps you load items into a ListBox or CheckedListBox.
	
	.DESCRIPTION
		Use this function to dynamically load items into the ListBox control.
	
	.PARAMETER ListBox
		The ListBox control you want to add items to.
	
	.PARAMETER Items
		The object or objects you wish to load into the ListBox's Items collection.
	
	.PARAMETER DisplayMember
		Indicates the property to display for the items in this control.
	
	.PARAMETER Append
		Adds the item(s) to the ListBox without clearing the Items collection.
	
	.EXAMPLE
		Update-ListBox $ListBox1 "Red", "White", "Blue"
	
	.EXAMPLE
		Update-ListBox $listBox1 "Red" -Append
		Update-ListBox $listBox1 "White" -Append
		Update-ListBox $listBox1 "Blue" -Append
	
	.EXAMPLE
		Update-ListBox $listBox1 (Get-Process) "ProcessName"
	
	.NOTES
		Additional information about the function.
#>
	
	param
	(
		[Parameter(Mandatory = $true)]
		[ValidateNotNull()]
		[System.Windows.Forms.ListBox]$ListBox,
		[Parameter(Mandatory = $true)]
		[ValidateNotNull()]
		$Items,
		[Parameter(Mandatory = $false)]
		[string]$DisplayMember,
		[switch]$Append
	)
	
	if (-not $Append)
	{
		$listBox.Items.Clear()
	}
	
	if ($Items -is [System.Windows.Forms.ListBox+ObjectCollection])
	{
		$listBox.Items.AddRange($Items)
	}
	elseif ($Items -is [Array])
	{
		$listBox.BeginUpdate()
		foreach ($obj in $Items)
		{
			$listBox.Items.Add($obj)
		}
		$listBox.EndUpdate()
	}
	else
	{
		$listBox.Items.Add($Items)
	}
	
	$listBox.DisplayMember = $DisplayMember
}
#endregion
